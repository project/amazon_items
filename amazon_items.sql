#
# Table structure for table `amazon_items`
#
CREATE TABLE `amazon_items` (
  `aid` int(10) unsigned NOT NULL default '0',
  `asin` varchar(10) NOT NULL default '0',
  `nid` int(10) unsigned NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `url` varchar(255) NOT NULL default '',
  `imgsmall` varchar(255) NOT NULL default '',
  `imgmedium` varchar(255) NOT NULL default '',
  `imglarge` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`aid`)
) TYPE=MyISAM;