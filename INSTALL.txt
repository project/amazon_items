INSTALL
=======

Installation instructions for Drupal 4.6.

Required:

1. Unpack the archive into your modules directory
2. Copy *.class.inc to the drupal includes directory
   (cp *.class.inc /your/drupal/dir/includes/)
   Note: Snoopy.class.inc is optional
3. Install the mysql file
   (mysql -u user -p database < amazon_items.sql)
4. Go to administer > modules and enable the amazon_items module
5. Go to administer > settings > amazon_items and fill in your affiliate ID
   and display preferences
 6. If you use Drupal 4.6, Go to administer > content > configure and click "configure" for the node
 types you want to enable to display Amazon products. Under "Workflow"
 you will see a blank check box with no label and no indication whatsoever
 of what that checkbox does. Check this box. If you use Drupal 4.5, Go to administer > content > configure > default workflow and put a check
   in the "amazon item" column for each node type that will accept Amazon
   products
7. Go to administer > users > configure > permissions and apply the "access
   amazon items" permission to at least one role

Optional:

* Go to administer > users > configure > permissions and apply the "administer
  amazon items" permission to the desired role(s) so they may add products to 
  nodes