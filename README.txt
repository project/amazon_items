README
======

The current maintainer of the Amazon Items module is Nikolaos S. Karastathis (NSK), http://nsk.wikinerds.org/

amazon_items
Allows sites to display products within nodes via the web services of Amazon.com.

Amazon items are products added to nodes via ASIN. They may be displayed in a 
box below node content, or in a sidebar block, or both.

The intended usage is to list items related to the node so that a reader could 
buy products from Amazon using your Affiliate ID, thus giving you a kickback on 
the purchase.

There are two ways to display items within nodes:

    * Box   - enable the option on admin/settings/amazon_items
    * Block - enable the block on admin/block

* See INSTALL.txt for complete installation instructions.
* Amazon help files from http://scripts.incutio.com/amazon/
  "I did some changes to it. I moved the xml parser init code from the
   constructor to the doSearch function. And I added extra search functionality,
   and paging support." -Breyten Ernsting (bje@dijkman.nl)
* Snoopy from http://snoopy.sourceforge.net/

Matt Grimm <slower@shamble.net>

NSK wishes to thank Matthew O'Malley for helping fixing the workflow bug in version 4.6.

Amazon Items is (C) 2004-2005 by the Amazon Items developers, including Matt Grimm and Nikolaos S. Karastathis. Contact: nsk@wikinerds.org